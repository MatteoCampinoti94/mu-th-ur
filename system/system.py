import os
import random
import time

import modules
from sounds import sounds
from terminal import terminal


class mu_th_ur():
    splash: list = [
        '        WEYLAN-YUTANI CORP        ',
        '    ___  ____  ____  ____  ___    ',
        '    \  \ \  / /    \ \  / /  /    ',
        '     \  \ \/ /  /\  \ \/ /  /     ',
        '      \  \  /  /  \  \  /  /      ',
        '       \  \/  / /\ \  \/  /       ',
        '        \____/ /__\ \____/        ',
        '     "Building Better Worlds"     ',
        '                                  ',
        '          MU/TH/UR  6000          ',
        '  OVER-MONITORING ADDRESS MATRIX  ',
        'SERIAL N. 78C30D926DB4BAE4392019F4',
    ]

    def __init__(self, *args, **kwargs):
        pass

    def start(self):
        sounds.tape.play()
        time.sleep(0.8)

        terminal.printLines(self.splash, centerh=True, centerv=True)

        time.sleep(4)
        sounds.tape.stop()

        for module in modules.modules_list:
            t = random.random() / 10
            terminal.printLoading(module[0], t, module[1], centerh=True)
            sounds.button.play()

        time.sleep(0.8)

        sounds.ready.play()
        print('\n\n')
        terminal.printLines(['[OK] SYSTEM READY           '], 0, centerh=True)
        print('\n' * 17)
        time.sleep(1.5)
        sounds.ready.stop()
