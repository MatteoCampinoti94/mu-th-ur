import pygame

pygame.init()


class sounds():
    tape = pygame.mixer.Sound('sounds/tape.wav')
    drive = pygame.mixer.Sound('sounds/drive.wav')
    ready = pygame.mixer.Sound('sounds/ready.wav')
    button = pygame.mixer.Sound('sounds/button.wav')
