from typing import *

from .interface import interface

modules_list: list = [
    ('CRFX         ', 'OM2077AM'),
    ('ATTITUDE     ', 'SM2078'),
    ('WASTE HEAT   ', '2080'),
    ('BAD          ', '2081'),
    ('VENT         ', '2082AM'),
    ('NAVIGATION   ', 'M2083'),
    ('TIME         ', 'M2084'),
    ('GAL POS      ', ''),
    ('COMMAND      ', '2086SC'),
    ('INTERFACE    ', '2037'),
    ('ATTN         ', '2087SC'),
    ('ALERT        ', '2088SC'),
    ('MARTIAL      ', '2090'),
    ('OVERLOCK     ', 'M2091'),
    ('L ALLIGNMENT ', 'SM2093'),
    ('PHOTO F      ', 'SM2094'),
    ('MAINS        ', ''),
    ('IUA          ', '2M2096'),
    ('2LA          ', '2M2097'),
    ('3RA          ', '2M2098'),
    ('4LHA         ', '2M2099'),
    ('GRAY GRIDS   ', ''),
    ('INERTIAL DAMP', '3002AM'),
    ('DECK A       ', 'A3003'),
    ('DECK B       ', 'A3004'),
    ('DECK C       ', 'A3005'),
    ('LIFE SUPPORT ', ''),
    ('0%           ', 'M3003AM')
]


def loadModule(mod: str) -> Optional[function]:

    if mod == 'ATTITUDE':
        pass
    elif mod == 'WASTE HEAT':
        pass
    elif mod == 'BAD':
        pass
    elif mod == 'VENT':
        pass
    elif mod == 'NAVIGATION':
        pass
    elif mod == 'TIME':
        pass
    elif mod == 'GAL POS':
        pass
    elif mod == 'COMMAND':
        pass
    elif mod == 'INTERFACE':
        return interface().query
    elif mod == 'ATTN':
        pass
    elif mod == 'ALERT':
        pass
    elif mod == 'MARTIAL':
        pass
    elif mod == 'OVERLOCK':
        pass
    elif mod == 'L ALLIGNMENT':
        pass
    elif mod == 'PHOTO F':
        pass
    elif mod == 'MAINS':
        pass
    elif mod == 'IUA':
        pass
    elif mod == '2LA':
        pass
    elif mod == '3RA':
        pass
    elif mod == '4LHA':
        pass
    elif mod == 'GRAY GRIDS':
        pass
    elif mod == 'INERTIAL DAMP':
        pass
    elif mod == 'DECK A':
        pass
    elif mod == 'DECK B':
        pass
    elif mod == 'DECK C':
        pass
    elif mod == 'LIFE SUPPORT':
        pass
    elif mod == '0%':
        pass

    return None
