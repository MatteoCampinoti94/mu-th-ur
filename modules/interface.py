import re
from typing import *


location = re.compile(r"^((where) ?(['i]s|are)|(what ?['i]s (the )?)?location( of)?) (the )?(ship|planet) ?([^\s?]+)?\??$")
mission = re.compile(r"^(what ?(['i]s|are) )?(the )?(mission|objectives?) ?((?<=mission )paramaters|priorities|objectives?)?\??$")


class interface():
    def __init__(self):
        self.queries: List[Tuple[str, str]] = []

    def query(self, q: str) -> str:
        q = q.lower()
        a = ''

        qre: List[str]

        if location.findall(q):
            qre = location.findall(q)
            q = f'location {qre[7]}[{qre[8]}]'

            for qs in self.queries:
                if q == qs[0]:
                    return qs[1]

            self.queries.append((q, a))
        elif mission.findall(q):
            qre = mission.findall(q)
            q = f'mission {qre[3]}[{qre[4]}]'

            for qs in self.queries:
                if q == qs[0]:
                    return qs[1]

            self.queries.append((q, a))
        else:
            a = 'DOES NOT COMPUTE'

        return a
