import os
import time
from typing import *

import readkeys


class terminal():
    def __init__(self):
        self

    @classmethod
    def screenClear(cls):
        return os.system('tput reset')

    @classmethod
    def screenSize(cls) -> Tuple[int, int]:
        return os.get_terminal_size()

    @classmethod
    def printLines(cls, lines: List[str], centerv: bool = False, centerh: bool = True):
        cols, lins = cls.screenSize()

        if centerv:
            print('\n' * ((lins // 2) - len(lines)))

        for line in lines:
            print(f'{line: ^{cols}}' if centerh else line)

    @classmethod
    def printLoading(cls, s1: str, t: float, s2: str = '', centerv: bool = False, centerh: bool = True):
        cols, lins = cls.screenSize()

        if centerv:
            print('\n' * ((lins // 2) - 1))

        if centerh:
            print('\n' + f'{"[//] LOADING: ": >{cols//2}}{s1}', end='\b' * (len(s1) + 14), flush=True)
        else:
            print('\n' + '[//] LOADING: ' + s1, end='\b' * (len(s1) + 14), flush=True)

        t = time.time() + t

        while time.time() < t:
            for l in ('-', '\\', '|', '/'):
                if time.time() > t: break
                time.sleep(0.1)
                print(f'[{l}{l}', end='\b\b\b', flush=True)

        print(f'[OK] LOADING: {s1} {s2}', end='\b' * (len(s1) + 14), flush=True)

    @classmethod
    def input(self, prompt: str = ''):
        try:
            return readkeys.input(prompt=prompt, key=str.upper).strip()
        except (EOFError, KeyboardInterrupt):
            return None
